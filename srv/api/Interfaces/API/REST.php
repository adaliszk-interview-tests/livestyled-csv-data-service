<?php
namespace UserApi\Interfaces\API;

use Illuminate\Http\Response;

interface REST
{
    public function get(int $id): Response;
    public function getList(): Response;
    public function post(): Response;
    public function put(int $id): Response;
    public function patch(int $id): Response;
    public function delete(int $id): Response;
}