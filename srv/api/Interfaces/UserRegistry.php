<?php
namespace UserApi\Interfaces;

interface UserRegistry
{
    public function getAll(bool $fromCache = false): array;
    public function getById(int $id): array;
}
