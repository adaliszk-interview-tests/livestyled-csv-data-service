<?php
namespace UserApi\Models\CSV;

use Exception;

/**
 * Class File
 *
 * @package RestfullAPI\Models\CSV
 */
class File
{
    private $name;

    /**
     * File constructor.
     *
     * Check's the $fileName readability before using it and stores the location
     *
     * @param string $fileName
     * @throws Exception
     */
    public function __construct(string $fileName)
    {
        if (!file_exists($fileName) || !is_readable($fileName))
            throw new Exception("Datafile is not readable or missing: {$fileName}");

        $this->name = $fileName;
    }

    /**
     * Reading file as a stream and yielding the rows one-by-one
     *
     * Instead using fgetcsv or file_get_contents for bigger dataset it's better to read the file as a stream
     * and with yielding processing the rows on-the-fly so if the requested data is already present then the reading
     * can be stopped sooner.
     *
     * Also with this you use less memory.
     *
     * @return \Generator
     */
    public function getRows()
    {
        $handle = fopen($this->name, "r");

        while (($line = fgets($handle)) !== false) {
            yield $line;
        }

        fclose($handle);
    }
}