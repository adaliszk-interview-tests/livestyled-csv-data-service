<?php
namespace UserApi\Models\CSV;

use UserApi\Interfaces\Datasource\Row as RowInterface;

class Row implements RowInterface
{
    private $columns = [];

    public function __construct(string $rowContent, string $separator=',', string $enclosure='"', string $escape = '\\')
    {
        $this->columns = str_getcsv($rowContent, $separator, $enclosure, $escape);
    }

    public function getData(): iterable
    {
        return $this->columns;
    }

    public function get(int $idx)
    {
        return $this->columns[$idx];
    }

    public function __toString()
    {
        return json_encode($this->columns);
    }
}