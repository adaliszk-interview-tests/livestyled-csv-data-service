<?php
namespace UserApi\Models\Users;

use UserApi\Application;
use UserApi\Interfaces\UserRegistry;
use UserApi\Interfaces\Datasource\Datasource;
use UserApi\Interfaces\User;

use Illuminate\Support\Facades\Cache;

class Registry implements UserRegistry
{
    private $app;
    private static $users = [];
    private $userClassName;

    /** @var \UserApi\Models\CSV\Datasource $data */
    private $data;

    public function __construct(Datasource $dataSource, Application $app, User $user)
    {
        $dataSourceClassName = get_class($dataSource);
        $this->data = new $dataSourceClassName('users', $app);
        $this->app = $app;

        $serialized = Cache::get(get_class($this).'::users'); // Look the cache for the serialized cache
        if (!empty($serialized)) // Restore static values
        {
            $users = unserialize($serialized);
            self::$users = $users;
        }

        $this->userClassName = get_class($user);
    }

    /**
     * @param bool $fromCache
     *
     * @return array
     *
     */
    public function getAll(bool $fromCache = false): array
    {
        // Returning cached instances if it's presented already
        if ($fromCache && !empty(self::$users)) return self::$users;

        $userClassName = $this->userClassName;

        // Iterate trough the cursor
        foreach($this->data->getRows() as $rowData)
        {
            // Get the datas as array
            $data = $rowData->getData();
            $user = new $userClassName($data);
            $id = $user->getId();

            self::$users[$id] = $user;
        }

        return self::$users;
    }

    /**
     * Retrieving Single user by Id
     *
     * @param int $id
     *
     * @return array
     */
    public function getById(int $id): array
    {
        // If the user already exists just return with it
        if (isset(self::$users[$id])) return [self::$users[$id]];

        // Fetch all datas until doesn't find it
        foreach ($this->getAll(false) as $user) {
            if ($user->getId() === $id) return [$user];
        }

        // When still doesn't exist return an empty array
        return [];
    }

    /**
     * Upon destruction save the users state into the cache
     */
    function __destruct()
    {
        Cache::put(get_class($this).'::users', serialize(self::$users));
    }
}