<?php
namespace UserApi\Models\Users\CSV;

use UserApi\Models\Users\User as BaseUser;

class User extends BaseUser
{
    private $map = [
        0   =>  'id',
        1   =>  'forName',
        2   =>  'lastName',
    ];

    public function __construct(array $data = [])
    {
        // Initialise without data
        parent::__construct([]);

        if (!empty($data))
        {
            foreach($data as $idx => $value)
            {
                $name = $this->map[$idx];
                if (in_array($name, $this->fillable))
                {
                    $this->$name = $value;
                }
            }

            foreach($this->fillable as $idx => $name)
                if (is_null($this->$name)) throw new UnexpectedValueException("Missing field value for '{$name}'");
        }
    }
}