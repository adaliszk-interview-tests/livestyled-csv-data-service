<?php
namespace UserApi\Models\Users;

use UserApi\Interfaces\User as UserInterface;
use UnexpectedValueException;

class User implements UserInterface
{
    /** @var int|null */
    protected $id = null;

    /** @var string|null */
    protected $forName = null;

    /** @var string|null */
    protected $lastName = null;

    /** @var array */
    protected $fillable = [
        'id', 'forName', 'lastName'
    ];

    /**
     * User class
     *
     * @param array $data Array of field values
     * @throws UnexpectedValueException when some required field is not provided
     */
    public function __construct(array $data = [])
    {
        if (!empty($data))
        {
            foreach($data as $name => $value)
                if (in_array($name, $this->fillable)) $this->$name = $value;

            foreach($this->fillable as $idx => $name)
                if (is_null($this->$name)) throw new UnexpectedValueException("Missing field value for '{$name}'");
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getForName(): string
    {
        return $this->forName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getName(): string
    {
        return "{$this->forName} {$this->lastName}";
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'forName' => $this->getForName(),
            'lastName' => $this->getLastName(),
            'name' => $this->getName()
        ];
    }
}