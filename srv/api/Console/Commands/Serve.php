<?php
namespace UserApi\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\ProcessUtils;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\PhpExecutableFinder;

use UserApi\Application;

/**
 * Class Serve
 *
 * @package Ionize\Console\Commands
 * @author: Ádám Liszkai <adaliszk@gmail.com>
 * @since: v2.0.0
 */
class Serve extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'serve';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Serve a PHP Built in Webserver';
    /**
     * Execute the console command.
     *
     * @param Application $app
     * @return void
     */
    public function fire(Application $app)
    {
        chdir($app->basePath());
        $host = $this->input->getOption('host');
        $port = $this->input->getOption('port');
        $base = ProcessUtils::escapeArgument($app->basePath());
        $binary = ProcessUtils::escapeArgument((new PhpExecutableFinder)->find(false));
        $this->info("Development server started on http://{$host}:{$port}/");
        passthru("{$binary} -S {$host}:{$port} {$base}/server");
    }
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            [
                'host', null,
                InputOption::VALUE_OPTIONAL,
                'The host address to serve the application on.',
                env('SERVER_HOSTNAME', 'localhost')
            ],
            [
                'port', null,
                InputOption::VALUE_OPTIONAL,
                'The port to serve the application on.',
                env('SERVER_PORT', 8080)
            ],
        ];
    }
}
/* End of file Serve.php */
/* Location: Ionize\Console\Commands */