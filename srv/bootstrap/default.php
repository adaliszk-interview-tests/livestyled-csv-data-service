<?php
require_once __DIR__ . '/../../vendor/autoload.php';

if (class_exists('Dotenv\Dotenv',true))
{
    try
    {
        (new Dotenv\Dotenv(realpath(__DIR__.'/../../')))->load();
    }
    catch (Dotenv\Exception\InvalidPathException $e)
    {
        error_log('Couldn\'t load .env file! '.$e->getMessage());
    }
}
#else error_log('Dotenv is not installed!', E_NOTICE);

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new UserApi\Application();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    UserApi\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    UserApi\Console\Kernel::class
);

$app->singleton(
    UserApi\Interfaces\UserRegistry::class,
    UserApi\Models\Users\Registry::class
);

$app->singleton(
    UserApi\Interfaces\Datasource\Datasource::class,
    UserApi\Models\CSV\Datasource::class
);

$app->singleton(
    UserApi\Interfaces\User::class,
    UserApi\Models\Users\CSV\User::class
);

/*
|--------------------------------------------------------------------------
| Register Middlewares
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
   UserApi\Middlewares\Output::class
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

// $app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'UserApi\Controllers'], function ($app) {
    require __DIR__ . '/../routes/api.php';
});

return $app;