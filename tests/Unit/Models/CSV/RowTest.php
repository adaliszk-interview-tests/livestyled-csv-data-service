<?php
namespace UserApi\Tests\Unit\Models\CSV;

use UserApi\Tests\TestCase;
use UserApi\Models\CSV\Row;

use ReflectionClass;
use Mockery;

class RowTest extends TestCase
{
    private $sampleData = '1,Ádám,Liszkai'.PHP_EOL;

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }

    public function getRequiredMethods(): array
    {
        $data = str_replace(PHP_EOL,'', $this->sampleData);
        $data = explode(',',$data);

        return [
            'getData method' => ['getData',[],$data],
            'get method' => ['get',[0],'1'],
        ];
    }

    /**
     * Testing that the class has the required methods for the test
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasRequiredMethod(string $methodName, array $params, $expectedValue)
    {
        $class = new Row($this->sampleData);
        $className = get_class($class);

        $reflection = new ReflectionClass($className);

        $this->assertTrue(
            $reflection->hasMethod($methodName),
            "Missing {$methodName} in {$className}"
        );
    }

    /**
     * Testing that the class methods has any output
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @depends hasRequiredMethod
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasReturnValue(string $methodName, array $params, $expectedValue)
    {
        echo PHP_EOL."--- hasReturnValue( $methodName )".PHP_EOL;

        $class = new Row($this->sampleData);
        $className = get_class($class);

        $returned = $class->$methodName(...$params);
        echo 'Returned: '.print_r($returned, true).PHP_EOL;

        $this->assertNotNull(
            $class->$methodName(...$params),
            "{$methodName} doesn't have any output in {$className}"
        );
    }

    /**
     * Testing that the class methods has the correct
     *
     * @param string $methodName
     * @param array $params
     * @param mixed $expectedValue
     *
     * @depends hasReturnValue
     * @dataProvider getRequiredMethods
     * @test
     */
    public function hasCorrectReturnValue(string $methodName, array $params, $expectedValue)
    {
        echo PHP_EOL."--- hasCorrectReturnValue( $methodName )".PHP_EOL;

        $class = new Row($this->sampleData);
        $className = get_class($class);

        $returned = $class->$methodName(...$params);
        echo 'Expected: '.print_r($expectedValue, true).PHP_EOL;
        echo 'Returned: '.print_r($returned, true).PHP_EOL;

        $this->assertSame($expectedValue, $returned);
    }
}
